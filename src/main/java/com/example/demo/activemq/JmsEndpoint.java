package com.example.demo.activemq;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.dsl.IntegrationFlowAdapter;
import org.springframework.integration.dsl.IntegrationFlowDefinition;
import org.springframework.integration.dsl.channel.MessageChannels;
import org.springframework.integration.dsl.jms.Jms;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.jms.support.converter.MarshallingMessageConverter;
import org.springframework.jms.support.destination.DynamicDestinationResolver;
import org.springframework.messaging.MessageChannel;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

public class JmsEndpoint extends IntegrationFlowAdapter {

    private String queueName;

    private String channelName;

    private String contextPath;

    /**
     * @param queueName
     * @param channelName
     * @param contextPath
     */
    public JmsEndpoint(String queueName, String channelName, String contextPath) {
        this.queueName = queueName;
        this.channelName = channelName;
        this.contextPath = contextPath;
    }

    @Override
    protected IntegrationFlowDefinition<?> buildFlow() {
        return from(Jms.messageDrivenChannelAdapter(listenerContainer())
                .jmsMessageConverter(new MarshallingMessageConverter(shipOrdersMarshaller()))
        ).channel(channelName);
    }

    @Bean
    public Jaxb2Marshaller shipOrdersMarshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath(contextPath);
        return marshaller;
    }

    @Bean
    public DynamicDestinationResolver dynamicDestinationResolver() {
        return new DynamicDestinationResolver();
    }

    @Bean
    public ActiveMQConnectionFactory connectionFactory() {
        return new ActiveMQConnectionFactory();
    }

    @Bean
    public DefaultMessageListenerContainer listenerContainer() {
        final DefaultMessageListenerContainer defaultMessageListenerContainer = new DefaultMessageListenerContainer();
        defaultMessageListenerContainer.setDestinationResolver(dynamicDestinationResolver());
        defaultMessageListenerContainer.setConnectionFactory(connectionFactory());
        defaultMessageListenerContainer.setDestinationName(queueName);
        return defaultMessageListenerContainer;
    }

    @Bean
    public MessageChannel inboundChannel() {
        return MessageChannels.direct(channelName).get();
    }
}
