package com.example.demo.activemq;

import com.example.stubs.PurchaseOrder;
import com.example.stubs.Shiporder;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.TransportConnector;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.channel.MessageChannels;
import org.springframework.integration.dsl.jms.Jms;
import org.springframework.integration.dsl.jms.JmsInboundGateway;
import org.springframework.integration.dsl.jms.JmsMessageDrivenChannelAdapter;
import org.springframework.integration.jms.ChannelPublishingJmsMessageListener;
import org.springframework.integration.xml.transformer.MarshallingTransformer;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.jms.support.converter.MarshallingMessageConverter;
import org.springframework.jms.support.destination.DynamicDestinationResolver;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import javax.xml.parsers.ParserConfigurationException;
import java.net.URI;

/**
 * Created by tomask79 on 20.12.17.
 */
@Configuration
public class ActiveMQConfiguration {

    @Bean
    public BrokerService createBrokerService() throws Exception {
        BrokerService broker = new BrokerService();
        TransportConnector connector = new TransportConnector();
        connector.setUri(new URI("tcp://localhost:61616"));
        broker.addConnector(connector);
        return broker;
    }

    @Bean
    public JmsEndpoint jmsEndpoint() {
        return new JmsEndpoint("jms.activeMQ.Test", "inboundChannel", "com.example.stubs");
    }

    /** uncomment when testing samples 1-4
    @Bean
    public DynamicDestinationResolver dynamicDestinationResolver() {
        return new DynamicDestinationResolver();
    }

    @Bean
    public ActiveMQConnectionFactory connectionFactory() {
        return new ActiveMQConnectionFactory();
    }

    @Bean
    public DefaultMessageListenerContainer listenerContainer() {
        final DefaultMessageListenerContainer defaultMessageListenerContainer = new DefaultMessageListenerContainer();
        defaultMessageListenerContainer.setDestinationResolver(dynamicDestinationResolver());
        defaultMessageListenerContainer.setConnectionFactory(connectionFactory());
        defaultMessageListenerContainer.setDestinationName("jms.activeMQ.Test");
        return defaultMessageListenerContainer;
    }

    @Bean
    public MessageChannel inboundChannel() {
        return MessageChannels.direct("inboundChannel").get();
    }
    */

    /**
     * Sample 1: Jms Inbound gateway
    @Bean
    public JmsInboundGateway dataEndpoint() {
        return Jms.inboundGateway(listenerContainer())
                .requestChannel(inboundChannel()).get();
    }
    **/

    /**
     * Sample 2: Jms Message driven adapter
    @Bean
    public JmsMessageDrivenChannelAdapter dataEndpoint() {
        final ChannelPublishingJmsMessageListener channelPublishingJmsMessageListener =
                new ChannelPublishingJmsMessageListener();
        channelPublishingJmsMessageListener.setExpectReply(false);
        final JmsMessageDrivenChannelAdapter messageDrivenChannelAdapter = new
                JmsMessageDrivenChannelAdapter(listenerContainer(), channelPublishingJmsMessageListener
                );

        messageDrivenChannelAdapter.setOutputChannel(inboundChannel());
        return messageDrivenChannelAdapter;
    }*/

    /**
     * Sample 3: Jms message driven adapter with JAXB
    @Bean
    public JmsMessageDrivenChannelAdapter dataEndpoint() {
        final ChannelPublishingJmsMessageListener channelPublishingJmsMessageListener =
                new ChannelPublishingJmsMessageListener();
        channelPublishingJmsMessageListener.setExpectReply(false);
        channelPublishingJmsMessageListener.setMessageConverter(new MarshallingMessageConverter(shipOrdersMarshaller()));
        final JmsMessageDrivenChannelAdapter messageDrivenChannelAdapter = new
                JmsMessageDrivenChannelAdapter(listenerContainer(), channelPublishingJmsMessageListener
        );

        messageDrivenChannelAdapter.setOutputChannel(inboundChannel());
        return messageDrivenChannelAdapter;
    }

    @Bean
    public Jaxb2Marshaller shipOrdersMarshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("com.example.stubs");
        return marshaller;
    }
     */

    /**
     * Sample 4: Jms message driven adapter with Jaxb and Payload routing.
     * @return
    @Bean
    public Jaxb2Marshaller ordersMarshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("com.example.stubs");
        return marshaller;
    }

    @Bean
    public JmsMessageDrivenChannelAdapter dataEndpoint() {
        final ChannelPublishingJmsMessageListener channelPublishingJmsMessageListener =
                new ChannelPublishingJmsMessageListener();
        channelPublishingJmsMessageListener.setMessageConverter(new MarshallingMessageConverter(ordersMarshaller()));
        final JmsMessageDrivenChannelAdapter messageDrivenChannelAdapter = new
                JmsMessageDrivenChannelAdapter(listenerContainer(), channelPublishingJmsMessageListener
        );

        messageDrivenChannelAdapter.setOutputChannel(inboundChannel());
        return messageDrivenChannelAdapter;
    }

    @Bean
    public IntegrationFlow payloadRootMapping() {
        return IntegrationFlows.from(inboundChannel()).<Object, Class<?>>route(Object::getClass, m->m
                .subFlowMapping(Shiporder.class, sf->sf.handle((MessageHandler) message -> {
                    final Shiporder shiporder = (Shiporder) message.getPayload();
                    System.out.println(shiporder.getOrderperson());
                    System.out.println(shiporder.getOrderid());
                }))
                .subFlowMapping(PurchaseOrder.class, sf->sf.handle((MessageHandler) message -> {
                    final PurchaseOrder purchaseOrderType = (PurchaseOrder) message.getPayload();
                    System.out.println(purchaseOrderType.getBillTo().getName());
                }))
        ).get();
    }
     */

}
