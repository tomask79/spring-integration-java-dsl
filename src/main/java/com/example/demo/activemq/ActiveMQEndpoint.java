package com.example.demo.activemq;

import com.example.stubs.Shiporder;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Service;

/**
 * Created by tomask79 on 20.12.17.
 */
@Service
public class ActiveMQEndpoint {

    /**
     * Samples 1 , 2
     *
    @ServiceActivator(inputChannel = "inboundChannel")
    public void processMessage(final String inboundPayload) {
        System.out.println("Inbound message: "+inboundPayload);
    }
     */

    /**
     * Sample 3, 5
     * @param shiporder
     */
    @ServiceActivator(inputChannel = "inboundChannel")
    public void processMessage(final Shiporder shiporder) {
        System.out.println(shiporder.getOrderid());
        System.out.println(shiporder.getOrderperson());
    }
}
