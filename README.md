# Spring Integration Java DSL #

This time I decided to play a little bit with 
[Spring Integration Java DSL](https://docs.spring.io/spring-integration/reference/html/java-dsl.html). 
Which has been merged directly into 
[Spring Integration Core 5.0](https://docs.spring.io/spring-integration/reference/html/whats-new.html), 
which is smart and obvious move because:

* Everyone starting the new Spring projects based on Java Config uses that
* SI Java DSL enables you to use new powerfull Java 8 features like Lambdas
* You can build your flow using the [Builder pattern](https://en.wikipedia.org/wiki/Builder_pattern) 
  based on [IntegrationFlowBuilder](https://docs.spring.io/autorepo/docs/spring-integration-java-dsl/1.1.0.M2/api/org/springframework/integration/dsl/IntegrationFlowBuilder.html)

Let's take a look on the samples howto use that based on ActiveMQ JMS.

Maven Dependencies:

	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-activemq</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.integration</groupId>
			<artifactId>spring-integration-core</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.integration</groupId>
			<artifactId>spring-integration-jms</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.apache.activemq</groupId>
			<artifactId>activemq-kahadb-store</artifactId>
		</dependency>

		<!-- https://mvnrepository.com/artifact/org.springframework.integration/spring-integration-java-dsl -->
		<dependency>
			<groupId>org.springframework.integration</groupId>
			<artifactId>spring-integration-java-dsl</artifactId>
			<version>1.2.3.RELEASE</version>
		</dependency>
	</dependencies>

## Sample 1: Jms inbound gateway ##

Let's have following [ServiceActivator](http://www.enterpriseintegrationpatterns.com/patterns/messaging/MessagingAdapter.html):

	@Service
	public class ActiveMQEndpoint {
    	@ServiceActivator(inputChannel = "inboundChannel")
    	public void processMessage(final String inboundPayload) {
        	System.out.println("Inbound message: "+inboundPayload);
    	}
	}

If you want to send the inboundPayload from Jms queue to this activator in the [Gateway](http://www.enterpriseintegrationpatterns.com/patterns/messaging/MessagingGateway.html) style with SI Java DSL 
then go with DSL [Jms](https://docs.spring.io/autorepo/docs/spring-integration-java-dsl/1.1.2.RELEASE/api/org/springframework/integration/dsl/jms/Jms.html) factory:

    @Bean
    public DynamicDestinationResolver dynamicDestinationResolver() {
        return new DynamicDestinationResolver();
    }

    @Bean
    public ActiveMQConnectionFactory connectionFactory() {
        return new ActiveMQConnectionFactory();
    }

    @Bean
    public DefaultMessageListenerContainer listenerContainer() {
        final DefaultMessageListenerContainer defaultMessageListenerContainer = new DefaultMessageListenerContainer();
        defaultMessageListenerContainer.setDestinationResolver(dynamicDestinationResolver());
        defaultMessageListenerContainer.setConnectionFactory(connectionFactory());
        defaultMessageListenerContainer.setDestinationName("jms.activeMQ.Test");
        return defaultMessageListenerContainer;
    }

    @Bean
    public MessageChannel inboundChannel() {
        return MessageChannels.direct("inboundChannel").get();
    }

    @Bean
    public JmsInboundGateway dataEndpoint() {
        return Jms.inboundGateway(listenerContainer())
                .requestChannel(inboundChannel()).get();
    }

returned [JmsInboundGatewaySpec](https://docs.spring.io/autorepo/docs/spring-integration-java-dsl/1.1.2.RELEASE/api/org/springframework/integration/dsl/jms/JmsOutboundGatewaySpec.html) 
by the dataEndpoint bean gives you also possibility to send reply to SI channel or Jms destination. Check the documentation.

## Sample 2: Jms message driven channel adapter ##

If you're looking for replacement of XML JMS configuration for message-driven-channel-adapter then 
[JmsMessageDrivenChannelAdapter](https://docs.spring.io/autorepo/docs/spring-integration-java-dsl/1.2.0.M1/api/org/springframework/integration/dsl/jms/JmsMessageDrivenChannelAdapter.html) is a way for you:


	@Bean
    public DynamicDestinationResolver dynamicDestinationResolver() {
        return new DynamicDestinationResolver();
    }

    @Bean
    public ActiveMQConnectionFactory connectionFactory() {
        return new ActiveMQConnectionFactory();
    }

    @Bean
    public DefaultMessageListenerContainer listenerContainer() {
        final DefaultMessageListenerContainer defaultMessageListenerContainer = new DefaultMessageListenerContainer();
        defaultMessageListenerContainer.setDestinationResolver(dynamicDestinationResolver());
        defaultMessageListenerContainer.setConnectionFactory(connectionFactory());
        defaultMessageListenerContainer.setDestinationName("jms.activeMQ.Test");
        return defaultMessageListenerContainer;
    }

    @Bean
    public MessageChannel inboundChannel() {
        return MessageChannels.direct("inboundChannel").get();
    }

    @Bean
    public JmsMessageDrivenChannelAdapter dataEndpoint() {
        final ChannelPublishingJmsMessageListener channelPublishingJmsMessageListener =
                new ChannelPublishingJmsMessageListener();
        channelPublishingJmsMessageListener.setExpectReply(false);
        final JmsMessageDrivenChannelAdapter messageDrivenChannelAdapter = new
                JmsMessageDrivenChannelAdapter(listenerContainer(), channelPublishingJmsMessageListener
                );

        messageDrivenChannelAdapter.setOutputChannel(inboundChannel());
        return messageDrivenChannelAdapter;
    }

as in the previous examples, inbound payload is send to the activator as in the sample 1.

## Sample 3: Jms message driven channel adapter with JAXB ##

In the typical scenario you want to accept XML through Jms as text message, convert it into JAXB stub and process it in the service activator.
I'm going to show you howto do this with SI Java DSL, but first let's add two dependencies for xml processing:

        <dependency>
			<groupId>org.springframework.integration</groupId>
			<artifactId>spring-integration-xml</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-oxm</artifactId>
		</dependency>


We're going to be accepting shiporders through JMS, so first XSD named **shiporder.xsd**:

    <?xml version="1.0" encoding="UTF-8" ?>
    <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

        <xs:element name="shiporder">
            <xs:complexType>
                <xs:sequence>
                    <xs:element name="orderperson" type="xs:string"/>
                    <xs:element name="shipto">
                        <xs:complexType>
                            <xs:sequence>
                                <xs:element name="name" type="xs:string"/>
                                <xs:element name="address" type="xs:string"/>
                                <xs:element name="city" type="xs:string"/>
                                <xs:element name="country" type="xs:string"/>
                            </xs:sequence>
                        </xs:complexType>
                    </xs:element>
                    <xs:element name="item" maxOccurs="unbounded">
                        <xs:complexType>
                            <xs:sequence>
                                <xs:element name="title" type="xs:string"/>
                                <xs:element name="note" type="xs:string" minOccurs="0"/>
                                <xs:element name="quantity" type="xs:positiveInteger"/>
                                <xs:element name="price" type="xs:decimal"/>
                            </xs:sequence>
                        </xs:complexType>
                    </xs:element>
                </xs:sequence>
                <xs:attribute name="orderid" type="xs:string" use="required"/>
            </xs:complexType>
        </xs:element>

    </xs:schema>

Now add JAXB maven plugin for generating JAXB stubs:

            <plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>jaxb2-maven-plugin</artifactId>
				<version>2.3.1</version>
				<executions>
					<execution>
						<id>xjc-schema1</id>
						<goals>
							<goal>xjc</goal>
						</goals>
						<configuration>
							<!-- Use all XSDs under the west directory for sources here. -->
							<sources>
								<source>src/main/resources/xsds/shiporder.xsd</source>
							</sources>

							<!-- Package name of the generated sources. -->
							<packageName>com.example.stubs</packageName>
							<outputDirectory>src/main/java</outputDirectory>
							<clearOutputDir>false</clearOutputDir>
						</configuration>
					</execution>
				</executions>
			</plugin>


We've got stubs classes and everything ready, now ** Java DSL JMS message driven adapter with Jaxb magic **:

    /**
     * Sample 3: Jms message driven adapter with JAXB
     */
    @Bean
    public JmsMessageDrivenChannelAdapter dataEndpoint() {
        final ChannelPublishingJmsMessageListener channelPublishingJmsMessageListener =
                new ChannelPublishingJmsMessageListener();
        channelPublishingJmsMessageListener.setExpectReply(false);
        channelPublishingJmsMessageListener.setMessageConverter(new MarshallingMessageConverter(shipOrdersMarshaller()));
        final JmsMessageDrivenChannelAdapter messageDrivenChannelAdapter = new
                JmsMessageDrivenChannelAdapter(listenerContainer(), channelPublishingJmsMessageListener
        );

        messageDrivenChannelAdapter.setOutputChannel(inboundChannel());
        return messageDrivenChannelAdapter;
    }

    @Bean
    public Jaxb2Marshaller shipOrdersMarshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("com.example.stubs");
        return marshaller;
    }

Nothing against XML configuration, but having this in Java gives you so much power and flexibility.
To complete this sample, service activator for inboundChannel will look like:

    /**
     * Sample 3
     * @param shiporder
     */
    @ServiceActivator(inputChannel = "inboundChannel")
    public void processMessage(final Shiporder shiporder) {
        System.out.println(shiporder.getOrderid());
        System.out.println(shiporder.getOrderperson());
    }
    
to test the flow, you can use following XML for sending into JMS queue through JConsole:

        <?xml version="1.0" encoding="UTF-8"?>        
        <shiporder orderid="889923"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:noNamespaceSchemaLocation="shiporder.xsd">
          <orderperson>John Smith</orderperson>
            <shipto>
              <name>Ola Nordmann</name>
              <address>Langgt 23</address>
              <city>4000 Stavanger</city>
              <country>Norway</country>
            </shipto>
            <item>
              <title>Empire Burlesque</title>
              <note>Special Edition</note>
              <quantity>1</quantity>
              <price>10.90</price>
              </item>
            <item>
              <title>Hide your heart</title>
              <quantity>1</quantity>
              <price>9.90</price>
            </item>
        </shiporder>
        
for a quick overview of howto use ActiveMQ with JConsole check this [tutorial](http://soatechlab.blogspot.cz/2008/01/use-jconsole-with-activemq-for-quick.html)

## Sample 4: Jms message driven channel adapter with JAXB and payload root routing ##

Another typical scenario is to accept XML as JMS text message, transform it into JAXB stub and route the payload to some
service activator according the payload root type. Of course SI Java DSL supports all types of routings, I'm going to show 
you **howto route according the payload type**.

First, add the following XSD into the same folder as shiporder.xsd and call it **purchaseorder.xsd**:

    <xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:tns="http://tempuri.org/PurchaseOrderSchema.xsd"
                targetNamespace="http://tempuri.org/PurchaseOrderSchema.xsd"
                elementFormDefault="qualified">
        <xsd:element name="PurchaseOrder">
            <xsd:complexType>
                <xsd:sequence>
                    <xsd:element name="ShipTo" type="tns:USAddress" maxOccurs="2"/>
                    <xsd:element name="BillTo" type="tns:USAddress"/>
                </xsd:sequence>
                <xsd:attribute name="OrderDate" type="xsd:date"/>
            </xsd:complexType>
        </xsd:element>

        <xsd:complexType name="USAddress">
            <xsd:sequence>
                <xsd:element name="name"   type="xsd:string"/>
                <xsd:element name="street" type="xsd:string"/>
                <xsd:element name="city"   type="xsd:string"/>
                <xsd:element name="state"  type="xsd:string"/>
                <xsd:element name="zip"    type="xsd:integer"/>
            </xsd:sequence>
            <xsd:attribute name="country" type="xsd:NMTOKEN" fixed="US"/>
        </xsd:complexType>
    </xsd:schema>

then add it to jaxb maven plugin configuration:
            
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>jaxb2-maven-plugin</artifactId>
				<version>2.3.1</version>
				<executions>
					<execution>
						<id>xjc-schema1</id>
						<goals>
							<goal>xjc</goal>
						</goals>
						<configuration>
							<!-- Use all XSDs under the west directory for sources here. -->
							<sources>
								<source>src/main/resources/xsds/shiporder.xsd</source>
								<source>src/main/resources/xsds/purchaseorder.xsd</source>
							</sources>

							<!-- Package name of the generated sources. -->
							<packageName>com.example.stubs</packageName>
							<outputDirectory>src/main/java</outputDirectory>
							<clearOutputDir>false</clearOutputDir>
						</configuration>
					</execution>
				</executions>
			</plugin>

run mvn clean install to generate the JAXB stubs for the new XSD. 
Now promised payload root mapping:

    @Bean
    public Jaxb2Marshaller ordersMarshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("com.example.stubs");
        return marshaller;
    }

    /**
     * Sample 4: Jms message driven adapter with Jaxb and Payload routing.
     * @return
     */
    @Bean
    public JmsMessageDrivenChannelAdapter dataEndpoint() {
        final ChannelPublishingJmsMessageListener channelPublishingJmsMessageListener =
                new ChannelPublishingJmsMessageListener();
        channelPublishingJmsMessageListener.setMessageConverter(new MarshallingMessageConverter(ordersMarshaller()));
        final JmsMessageDrivenChannelAdapter messageDrivenChannelAdapter = new
                JmsMessageDrivenChannelAdapter(listenerContainer(), channelPublishingJmsMessageListener
        );

        messageDrivenChannelAdapter.setOutputChannel(inboundChannel());
        return messageDrivenChannelAdapter;
    }

    @Bean
    public IntegrationFlow payloadRootMapping() {
        return IntegrationFlows.from(inboundChannel()).<Object, Class<?>>route(Object::getClass, m->m
                .subFlowMapping(Shiporder.class, sf->sf.handle((MessageHandler) message -> {
                    final Shiporder shiporder = (Shiporder) message.getPayload();
                    System.out.println(shiporder.getOrderperson());
                    System.out.println(shiporder.getOrderid());
                }))
                .subFlowMapping(PurchaseOrder.class, sf->sf.handle((MessageHandler) message -> {
                    final PurchaseOrder purchaseOrderType = (PurchaseOrder) message.getPayload();
                    System.out.println(purchaseOrderType.getBillTo().getName());
                }))
        ).get();
    }

Notice the payloadRootMapping bean, let's explain the important parts:

* <Object, Class<?>>route - says that input from inboundChannel will be Object and routing will be performed according the Class<?>
* subFlowMapping(Shiporder.class... - processing of ShipOders.
* subFlowMapping(PurchaseOrder.class... - processing of PurchaseOrders.

for testing the ShipOrder payload use the XML from Sample 3, for testing the PurchaseOrder payload use the following XML:

    <?xml version="1.0" encoding="utf-8"?>  
    <PurchaseOrder OrderDate="1900-01-01" xmlns="http://tempuri.org/PurchaseOrderSchema.xsd">  
      <ShipTo country="US">  
        <name>name1</name>  
        <street>street1</street>  
        <city>city1</city>  
        <state>state1</state>  
        <zip>1</zip>  
      </ShipTo>  
      <ShipTo country="US">  
        <name>name2</name>  
        <street>street2</street>  
        <city>city2</city>  
        <state>state2</state>  
        <zip>-79228162514264337593543950335</zip>  
      </ShipTo>  
      <BillTo country="US">  
        <name>name1</name>  
        <street>street1</street>  
        <city>city1</city>  
        <state>state1</state>  
        <zip>1</zip>  
      </BillTo>  
    </PurchaseOrder>  

both payloads should be routed according the subflow mapping.

## Sample 5: IntegrationFlowAdapter ##

Except of other implementation of Enterprise Integration Patterns (check them [out](https://docs.spring.io/autorepo/docs/spring-integration-java-dsl/1.1.0.M2/api/org/springframework/integration/dsl/IntegrationFlowBuilder.html)) 
I need to mention **IntegrationFlowAdapter**. Very shortly, by extending this class and implementing the **buildFlow method** like:

  @Component
  public class MyFlowAdapter extends IntegrationFlowAdapter {

     @Autowired
     private ConnectionFactory rabbitConnectionFactory;

     @Override
     protected IntegrationFlowDefinition<?> buildFlow() {
          return from(Amqp.inboundAdapter(this.rabbitConnectionFactory, "myQueue"))
                   .<String, String>transform(String::toLowerCase)
                   .channel(c -> c.queue("myFlowAdapterOutput"));
     }

 }

you can wrap repeating declaration of beans into one component and give them desired flow.
Such component can be then configured and given to the calling code as one class instance!

So let's make for example sample 3 in this repo a little shorter and define base class for all JmsEndpoints
with repeating beans defined inside:

    public class JmsEndpoint extends IntegrationFlowAdapter {
    
        private String queueName;

        private String channelName;

        private String contextPath;

        /**
         * @param queueName
         * @param channelName
         * @param contextPath
         */
        public JmsEndpoint(String queueName, String channelName, String contextPath) {
            this.queueName = queueName;
            this.channelName = channelName;
            this.contextPath = contextPath;
        }

        @Override
        protected IntegrationFlowDefinition<?> buildFlow() {
            return from(Jms.messageDrivenChannelAdapter(listenerContainer())
                .jmsMessageConverter(new MarshallingMessageConverter(shipOrdersMarshaller()))
            ).channel(channelName);
        }

        @Bean
        public Jaxb2Marshaller shipOrdersMarshaller() {
            Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
            marshaller.setContextPath(contextPath);
            return marshaller;
        }

        @Bean
        public DynamicDestinationResolver dynamicDestinationResolver() {
            return new DynamicDestinationResolver();
        }

        @Bean
        public ActiveMQConnectionFactory connectionFactory() {
            return new ActiveMQConnectionFactory();
        }

        @Bean
        public DefaultMessageListenerContainer listenerContainer() {
            final DefaultMessageListenerContainer defaultMessageListenerContainer = new DefaultMessageListenerContainer();
            defaultMessageListenerContainer.setDestinationResolver(dynamicDestinationResolver());
            defaultMessageListenerContainer.setConnectionFactory(connectionFactory());
            defaultMessageListenerContainer.setDestinationName(queueName);
            return defaultMessageListenerContainer;
        }

        @Bean
        public MessageChannel inboundChannel() {
            return MessageChannels.direct(channelName).get();
        }
    }

now declaring of Jms endpoint for particular queue is easy as never:

    @Bean
    public JmsEndpoint jmsEndpoint() {
        return new JmsEndpoint("jms.activeMQ.Test", "inboundChannel", "com.example.stubs");
    }

service activator for inboundChannel:

    /**
     * Sample 3, 5
     * @param shiporder
     */
    @ServiceActivator(inputChannel = "inboundChannel")
    public void processMessage(final Shiporder shiporder) {
        System.out.println(shiporder.getOrderid());
        System.out.println(shiporder.getOrderperson());
    }
    
You should not miss using of IntegrationFlowAdapter in projects. I love it's concept.     

## Testing the code ##

In this repo you're going to find all of the samples. Focus on **ActiveMQEndpoint** and **ActiveMQConfiguration** classes and uncomment 
sample you wish to test.

## Summary ##

I started to use Spring Integration Java DSL at the new Spring Boot based project recently in [Embedit](https://www.embedit.cz/). 
Even with a few configurations I find it very usefull.

- It's easily debugable. Without adding configurations like wiretap.
- It's far more easily to read. Yes, even with lambdas!
- It's powerfull. In Java configuration you've got now many options.

I hope you found my samples usefull. See you.

regards

Tomas

